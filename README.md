# Hangman-rs
This is a rust port of hangman for cs.dsunix.net timeshare server

## Building
The project must be built for release with:
```sh
cargo build --release --target x86_64-unknown-linux-musl
```
Make sure the target is installed with:
```sh
rustup target add x86_64-unknown-linux-musl
```
